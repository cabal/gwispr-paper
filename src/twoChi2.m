function  selNew = twoChi2(list, ctFile, nCtrl, nCase)
    sel = txt2mat(fullfile(list));
    sel.prb = [sel.prb1, sel.prb2];

    ctOut = load(ctFile);

    [TF, loc] = ismember(sel.prb, ctOut.prb, 'rows');

    ctOut = rmfield(ctOut, 'jtag');
    ctOut = rmfield(ctOut, 'optionsStruct');
    ctOut = cutStruc2Inds(ctOut, loc(TF));
    
    assert(isequal(ctOut.prb, sel.prb))

    nPairs = size(ctOut.ct, 1);

    Ctrl = reshape(ctOut.ct(:,1:9)', [3, 3, nPairs]);
    Case = reshape(ctOut.ct(:,10:18)', [3, 3, nPairs]);

    Case = Case / nCase;
    Ctrl = Ctrl / nCtrl;

    margRowCtrl = sum(Ctrl, 2);
    margColCtrl = sum(Ctrl, 1);
    margRowCase = sum(Case, 2);
    margColCase = sum(Case, 1);

    VCtrl = zeros(3,3,nPairs);
    VCase = zeros(3,3,nPairs);

    c = 1/nCtrl + 1/nCase;

    for i = 1:3
        for j = 1:3
            VCtrl(i,j,:) = (Ctrl(i, j, :) - margRowCtrl(i, 1, :) .* margColCtrl(1, j, :)) ./ ...
                            sqrt(margRowCtrl(i, 1, :) .* margColCtrl(1, j, :) * c);
            VCase(i,j,:) = (Case(i, j, :) - margRowCase(i, 1, :) .* margColCase(1, j,:)) ./ ...
                            sqrt(margRowCase(i, 1, :) .* margColCase(1, j, :) * c);
        end
    end

    S = (VCtrl - VCase).^2;
    selNew = sel;
    selNew.twochi2 = squeeze(sum(sum(S,2), 1));
end 