function overlapPlots(disease)
    datapath = ['/home/herman/Documents/papers/gwispr-results/results_' disease];
    datasets = {'chi', 'ss', 'dss',  'fastepi', 'gboost', 'ig', 'eor', 'epiblaster','twochi2'};
    labels = {'Chi^2', 'SS', 'DSS', 'plink fastepi', 'GBOOST', 'Info. Gain', 'SHEsisEpi', 'Epiblaster', '2Chi^2'};
    colSty = {'b-', 'r-', 'g-', 'c-', 'm-', 'k-', 'b--', 'r--', 'g--'};
    subplotArrangement = [3, 3];
    LineWidth = 2;
    FontSize = 12;

    if ~exist(fullfile(datapath, [disease '_plotData.mat']), 'file')
        [K, C] = generateplotData(datapath, datasets);
        save(fullfile(datapath, [disease '_plotData.mat']), 'K', 'C');
    else
        plotDat = load(fullfile(datapath, [disease '_plotData.mat']));
        K = plotDat.K;
        C = plotDat.C;
    end

    figH = figure('units','normalized','outerposition',[0 0 1 1]);


    for I = 1:length(datasets)
        axH = subplot(subplotArrangement(1), subplotArrangement(2), I, 'parent', figH);
        for J = 1:length(datasets)
            if I == J
                continue;
            end
            loglog(axH, K, C{I,J}, colSty{J}, 'LineWidth', LineWidth); hold on;
        end
        title(axH, [labels{I} ' vs. '], 'FontSize', FontSize);
        ylabel('intersection %',  'FontSize', FontSize);
        xlabel('sorted list length', 'FontSize', FontSize);
        %set(axH, 'ylim', [0,100], 'FontSize', FontSize)
        set(axH, 'ylim', [1,100],  'FontSize', FontSize)
        set(axH, 'xlim', [1,10^5], 'FontSize', FontSize)
        set(axH, 'Xtick', 10.^[0:5]);
        grid on;


    end
    h = semilogx(axH, K, C{length(datasets),length(datasets)-1}./K * 100, colSty{length(datasets)});
    set(h, 'Visible', 'off');
    legend(labels, 'FontSize', FontSize);
    %text(215, 400, disease);
    %text(215, 4, disease);
    %text(215, 8.5e7, disease);
    text(5.6e-5, 9.2e7, disease);

    saveas(figH, fullfile(datapath, ['overlap_' disease '.fig']), 'fig');
    %saveas(figH, fullfile(datapath, ['overlap_' disease '.png']), 'png');
    print('-depsc', fullfile(datapath, ['overlap_' disease '.eps']), '-r1200')
end

function [kdx, C] = generateplotData(datapath, datasets)        
    for I = 1:length(datasets)
        data.(datasets{I}) = txt2mat(fullfile(datapath, [datasets{I} '.txt']));

        data.(datasets{I}).prb = sort([data.(datasets{I}).prb1, data.(datasets{I}).prb2], 2, 'ascend');

        d = data.(datasets{I});
        [A, idx] = sort(d.(datasets{I}), 'descend');

        data.(datasets{I}) = cutStruc2Inds(d, idx);
    end
    
    kdx = [1:100, 2.^(7:16)];
    C = {};
    
    for I = 1:length(datasets)
        [cutoff, idx] = unique(data.(datasets{I}).(datasets{I}), 'last');

        R = [];
        for J = 1:length(datasets)
            if I == J
                continue;
            end
            
            Y = zeros(1, length(kdx));


            for K = 1:length(kdx)
                Y(K) = nnz(ismember(data.(datasets{I}).prb(1:kdx(K), :), ...
                                        data.(datasets{J}).prb(1:kdx(K), :), 'rows'));

            end

            Y = Y./kdx * 100;
            %semilogx(axH, kdx, C, colSty{J}); hold on;

            Y(Y < 0.1) = 0.1;

            % take care of overlaps
            if ~isempty(R)
                for K = 1:length(Y)
                    TF = true;
                    while(TF)
                        TF = ismember(Y(K), R(:,K));
                        if TF
                            Y(K) = Y(K) * 1.1;
                        end
                    end
                end
                R = [R; Y];
            else
                R = Y;
            end

            C{I,J} = Y;
        end
    end
end

        
        
        
        
