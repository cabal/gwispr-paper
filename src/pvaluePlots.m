function pvaluePlots(disease)
    datapath = ['/home/herman/Documents/papers/gwispr-results/results_' disease];
    datasets = {'chi', 'ss',   'fastepi', 'gboost', 'eor', 'epiblaster','twochi2'};
    labels = {'Chi^2', 'SS',  'plink fastepi', 'GBOOST', 'SHEsisEpi', 'Epiblaster', '2Chi^2'};
    colSty = {'b-', 'r-',  'c-', 'm-', 'b--', 'r--', 'g--'};
    subplotArrangement = [3, 3];
    LineWidth = 2;
    FontSize = 12;

    if ~exist(fullfile(datapath, [disease '_pvalueData.mat']), 'file')
        [K, C] = generatePValues(datapath, datasets, disease);
        save(fullfile(datapath, [disease '_pvalueData.mat']), 'K', 'C');
    else
        plotDat = load(fullfile(datapath, [disease '_pvalueData.mat']));
        K = plotDat.K;
        C = plotDat.C;
    end

    figH = figure();

    for I = 1:length(datasets)
       h = loglog(K, C{I}, colSty{I}, 'LineWidth', LineWidth); hold on
       if (strcmp(datasets{I}, 'ig'))
           set(h, 'Visible', 'off');
       end
    end
    title(['pvalues for ' disease], 'FontSize', FontSize);
    ylabel('-log_{10}(p)',  'FontSize', FontSize);
    xlabel('sorted list length', 'FontSize', FontSize);
    set(gca, 'xlim', [1,10^5], 'FontSize', FontSize)
    set(gca, 'Xtick', 10.^[0:5]);
    grid on;
    
    legend(labels, 'FontSize', FontSize);
    
    saveas(figH, fullfile(datapath, ['pvalue_' disease '.fig']), 'fig');
    print('-depsc', fullfile(datapath, ['pvalue_' disease '.eps']), '-r1200')
end

function [kdx, C] = generatePValues(datapath, datasets, disease)        
    for I = 1:length(datasets)
        data.(datasets{I}) = txt2mat(fullfile(datapath, [datasets{I} '.txt']));

        data.(datasets{I}).prb = sort([data.(datasets{I}).prb1, data.(datasets{I}).prb2], 2, 'ascend');

        d = data.(datasets{I});
        [A, idx] = sort(d.(datasets{I}), 'descend');

        data.(datasets{I}) = cutStruc2Inds(d, idx);
    end
    
    
    kdx = [1:100, 2.^(7:16)];
    C = {};
    
    for I = 1:length(datasets)
        switch(datasets{I})
            case 'chi'
                X = -log10chi2(data.(datasets{I}).(datasets{I}), 8);
                C{I} = X(kdx);
            case {'ss', 'dss'}
                X = data.(datasets{I}).(datasets{I});
                C{I} = X(kdx);
            case {'fastepi', 'eor'}
                X = -log10chi2(data.(datasets{I}).(datasets{I}), 1);
                C{I} = X(kdx);
            case {'twochi2', 'gboost', 'ig'}
                X = -log10chi2(data.(datasets{I}).(datasets{I}), 4);
                C{I} = X(kdx);
            case 'epiblaster'
                var = 1/(2938-1);
                switch (disease)
                    case 'BD'
                        var = var + 1/(1868-1);
                    case 'CAD'
                        var = var + 1/(1926-1);
                    case 'CD'
                        var = var + 1/(1748-1);
                    case 'HT'
                        var = var + 1/(1952-1);
                    case 'RA'
                        var = var + 1/(1860-1);
                    case 'T1D'
                        var = var + 1/(1963-1);
                    case 'T2D'
                        var = var + 1/(1924-1);
                end
                X = -log10(0.5*(1 - erf(data.(datasets{I}).(datasets{I})/sqrt(2)/sqrt(var))));
                C{I} = X(kdx);
        end
    end
end

        
        
        
        
