function selNew = epiblaster( list, ctFile, nCtrl, nCase)
    sel = txt2mat(fullfile(list));
    sel.prb = [sel.prb1, sel.prb2];

    ctOut = load(ctFile);

    [TF, loc] = ismember(sel.prb, ctOut.prb, 'rows');

    ctOut = rmfield(ctOut, 'jtag');
    ctOut = rmfield(ctOut, 'optionsStruct');
    ctOut = cutStruc2Inds(ctOut, loc(TF));
    
    assert(isequal(ctOut.prb, sel.prb))

    nPairs = size(ctOut.ct, 1);

    Ctrl = reshape(ctOut.ct(:,1:9)', [3, 3, nPairs]);
    Case = reshape(ctOut.ct(:,10:18)', [3, 3, nPairs]);
    
    margRowCtrl = sum(Ctrl, 2);
    margColCtrl = sum(Ctrl, 1);
    margRowCase = sum(Case, 2);
    margColCase = sum(Case, 1);

    xCtrla = zeros(1, 1, nPairs); xCasea = zeros(1, 1, nPairs);
    xCtrlb = zeros(1, 1, nPairs); xCaseb = zeros(1, 1, nPairs);
    sCtrla = zeros(1, 1, nPairs); sCasea = zeros(1, 1, nPairs);
    sCtrlb = zeros(1, 1, nPairs); sCaseb = zeros(1, 1, nPairs);
    NCtrla = margRowCtrl(1, 1, :); NCasea = margRowCase(1, 1, :);
    NCtrlb = margColCtrl(1, 1, :); NCaseb = margColCase(1, 1, :);
    
    for i = 2:3
        NCtrla = NCtrla + margRowCtrl(i, 1, :); NCasea = NCasea + margRowCase(i, 1, :);
        NCtrlb = NCtrlb + margColCtrl(1, i, :); NCaseb = NCaseb + margColCase(1, i, :);
        
        TCtrla = repmat((i-1), [1, 1, nPairs]) - xCtrla; TCasea = repmat((i-1), [1, 1, nPairs]) - xCasea;
        TCtrlb = repmat((i-1), [1, 1, nPairs]) - xCtrlb; TCaseb = repmat((i-1), [1, 1, nPairs]) - xCaseb;
        
        xCtrla = xCtrla + (margRowCtrl(i, 1, :) .* TCtrla) ./ NCtrla; xCasea = xCasea + (margRowCase(i, 1, :) .* TCasea) ./ NCasea;
        xCtrlb = xCtrlb + (margColCtrl(1, i, :) .* TCtrlb) ./ NCtrlb; xCaseb = xCaseb + (margColCase(1, i, :) .* TCaseb) ./ NCaseb;
        
        sCtrla = sCtrla + margRowCtrl(i, 1, :) .* TCtrla .* ((i-1) - xCtrla); sCasea = sCasea + margRowCase(i, 1, :) .* TCasea .* ((i-1) - xCasea);
        sCtrlb = sCtrlb + margColCtrl(1, i, :) .* TCtrlb .* ((i-1) - xCtrlb); sCaseb = sCaseb + margColCase(1, i, :) .* TCaseb .* ((i-1) - xCaseb);
    end
    sCtrla = sqrt(sCtrla/(nCtrl-1)); sCasea = sqrt(sCasea/(nCase-1));
    sCtrlb = sqrt(sCtrlb/(nCtrl-1)); sCaseb = sqrt(sCaseb/(nCase-1));
    
    
    crosCtrl = zeros(1, 1, nPairs); crosCase = zeros(1, 1, nPairs);
    prodCtrl = zeros(1, 1, nPairs); prodCase = zeros(1, 1, nPairs);
    
    for i = 1:3
        crosCtrl = crosCtrl + (margRowCtrl(i, 1, :) .* xCtrlb  + margColCtrl(1,i,:) .* xCtrla) * (i-1);
        crosCase = crosCase + (margRowCase(i, 1, :) .* xCaseb  + margColCase(1,i,:) .* xCasea) * (i-1);
        for j = 1:3
            prodCtrl = prodCtrl + Ctrl(i, j, :) * (i-1) * (j-1);
            prodCase = prodCase + Case(i, j, :) * (i-1) * (j-1);
        end
    end
    
    CovCtrl = (prodCtrl - crosCtrl + xCtrla .* xCtrlb * nCtrl)/(nCtrl -1);
    CovCase = (prodCase - crosCase + xCasea .* xCaseb * nCase)/(nCase -1);
    rCtrl = CovCtrl ./ (sCtrla .* sCtrlb);
    rCase = CovCase ./ (sCasea .* sCaseb);

    selNew = sel;
    
    selNew.epiblaster = squeeze(rCase - rCtrl);
end

